Readme
------
This module allows users with the 'administer taxonomy' role to perform
batch operations on taxonomy terms.

Features include:

- a "add multiple terms" tab, in which multiple terms can be
entered or pasted into a single textarea, greatly decreasing the time
required to add many taxonomy terms to a vocabulary.

- a checkbox next to each taxonomy term and a "Delete Selected" button to
allow for quick deletion of multiple terms at once.

- the addition of a "delete" link in the Operations column of the list terms
table to allow for quick deletion of single terms and allowing
administrators to bypass the term edit screen.

- the addition of a column showing a tally of how many nodes make use of
each taxonomy term on the list terms page.

- clickable table headers in the list terms table allowing for easy sorting
of terms based on name (alphabetical and reverse alphabetical) or number of
associated nodes (ascending or descending) on the list terms page.

- hovering over a term name on the list terms page will provide a tooltip
with the term's associated description (if a description has been set for
that term).

Send comments to William Smith (aka drawk) at: http://drupal.org/user/68658/contact


Requirements
------------
This module requires Drupal 4.7, and the taxonomy module to be enabled.


Installation
------------
1. Copy the taxonomy_multiedit directory into your web site's /modules directory.
2. Activate the module by visiting http://www.example.com/admin/modules, where
   example.com is the URL of your web site.


Database Schema
---------------
This module does not create any additional database tables and does not
alter any existing tables.


Credits
-------
Written by William Smith.


TODO
----
1. Add intelligent sorting mechanism for hierarchical vocabularies
(keep children terms coupled with parent terms and sort each depth level
independently)

2. Make display more consistant with other areas of Drupal admin interface,
such as the admin comments page.  Checkboxes moved to the far left of table,
and "Delete Selected" renamed "Update", moved above the table, and next to a
dropdown list of possible actions.

3. When deleting multiple terms, confirmation page should list each term by
name.

4. Add "weight" column to the list term table, so that you can easily set all the
weights for your terms without bothering with the edit screen.  Add "Save
Weights" to dropdown list of actions next to "Update" button (see #2)
